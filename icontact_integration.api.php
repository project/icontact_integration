<?php

/**
 * @file
 * Document all supported APIs.
 */
 // hook_icontact_integration_user_data
function hook_icontact_integration_user_data_alter(&$data, &$user) {
   // here others will make a module that will call this to alter "$data"
}
 // hook_icontact_integration_user_data
function hook_icontact_integration_user_data(&$data, &$user) {
   // here others will make a module that will call this to alter "$data"
}