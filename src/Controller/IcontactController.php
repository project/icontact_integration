<?php

namespace Drupal\icontact_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;

/**
 * Returns responses for Metatag routes.
 */
class IcontactController extends ControllerBase {

	public function help($value='')
	{
		$get_help_info = get_help_info();
		$build = [
			'#markup' => $this->t($get_help_info),
		];
		return $build;
	}

	public function about($value='')
	{
		$about_icontact = about_icontact();
		$build = [
			'#markup' => $this->t($about_icontact),
		];
		return $build;
	}

}
