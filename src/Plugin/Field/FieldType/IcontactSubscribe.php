<?php

namespace Drupal\icontact_integration\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemBase;


/**
 * Defines the 'icontact_subscribe_field' entity field type.
 *
 * @FieldType(
 *   id = "icontact_subscribe_field",
 *   label = @Translation("iContact Subscribe"),
 *   module = "icontact_integration",
 *   description = @Translation("An entity field containing share on social media field."),
 *   default_widget = "icontact_subscribe_field_widget",
 * )
 */
class IcontactSubscribe extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['icontact_subscribe'] = DataDefinition::create('string')
    ->setLabel(t('iContact Subscribe'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'icontact_subscribe' => [
          'type' => 'text',
        ]
      ],
    ];
  }

  /**
   * If a max value is set to 0, remove it, otherwise set the value.
   *
   * The form that submits this is a bit weird so we need to pull the array out.
   */
  public function preSave() {
    parent::preSave();
    $value = $this->getValue();

    $data_value = $value['value'];
    $icontact_subscribe = $data_value['icontact_subscribe'];

    $this->setValue($icontact_subscribe);
  }

  public function getValues() {
    return $this->getValue();
  }

}
