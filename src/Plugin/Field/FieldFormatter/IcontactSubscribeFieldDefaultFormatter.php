<?php

namespace Drupal\icontact_integration\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;

/**
 * Plugin implementation of the 'text_default' formatter.
 *
 * @FieldFormatter(
 *   id = "icontact_subscribe_field_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "icontact_subscribe_field",
 *   }
 * )
 */
class IcontactSubscribeFieldDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // The ProcessedText element already handles cache context & tag bubbling.
    // @see \Drupal\filter\Element\ProcessedText::preRenderText()
    foreach ($items as $delta => $item) {

      $values = $item->getValues();
      if(!empty($values)) {
        $icontact_subscribe = (isset($values['icontact_subscribe']))?$values['icontact_subscribe']:'';
        if($icontact_subscribe != '') {
          $text .= '<h3>iContact Subscribe: '.$icontact_subscribe.'</h3>';
        }
        $elements[$delta] = [
          '#type' => 'processed_text',
          '#text' => $text,
          '#format' => 'full_html',
          '#langcode' => $item->getLangcode(),
        ];
      }
    }
    return $elements;
  }




}
