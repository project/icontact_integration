<?php

namespace Drupal\icontact_integration\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Plugin implementation of the 'icontact_subscribe_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "icontact_subscribe_field_widget",
 *   label = @Translation("iContact Subscribe Widget"),
 *   field_types = {
 *     "icontact_subscribe_field"
 *   }
 * )
 */
class IcontactSubscribeFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = $items[$delta]->getValue() !== NULL ? $items[$delta]->getValue() : '';

    $icontact_subscribe = $value['icontact_subscribe']??0;

    $element += [
      '#type' => 'value',
      '#element_validate' => [
        [$this, 'validate'],
      ],
      // '#default_value' => $value,
    ];

    $element['icontact_subscribe'] = [
      '#title' => t('iContact Subscribe'),
      '#type' => 'checkbox',
      '#default_value' => $icontact_subscribe,
    ];

    return ['value' => $element];
  }

  /**
   * {@inheritdoc}
   */
  public function validate($element, FormStateInterface $form_state)
  {
  }

}
