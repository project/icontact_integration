<?php

namespace Drupal\icontact_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\icontact_integration\Plugin\iContactApi;

/**
 * Settings form for Social Autopost.
 */
class IcontactMappingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['icontact_mappings.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'icontact_mappings.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('icontact_mappings.settings');

    $site_config = \Drupal::config('system.site');
    $site_name = $site_config->get('name');

    $field_keys = $this->get_field_keys();

    $help = Url::fromRoute('icontact_integration.help_guide',array(),array('absolute'=>'true'))->toString();

    $form['api_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Field Mapping Configuratios'),
      '#open' => TRUE,
      '#description' => $this->t('Note : You have to create a Field Mapping application before filling the following details. Click <a href="https://developer.twitter.com/en/apps/create">here</a> to create new Field Mapping application. '),
    ];

    $valid_field_types = array('string','string_long','email');
    $entity_type = 'user';
    $entity_bundle = 'user';

    $fields = get_entity_fields($entity_type, $entity_bundle, $valid_field_types);
    $form['api_settings']['email'] = [
      '#type' => 'select',
      '#title' => $this->t('Email'),
      '#options' => array('email'=>'Email'),
      '#default_value' => array('email'),
      '#description' => "Select the related field on the {$site_name} system for the iContact Email field",
      '#required' => TRUE,
    ];

    if(!empty($field_keys)) {
      foreach ($field_keys as $field_name) {
        $label = strtoupper($field_name);
        $form['api_settings'][$field_name] = [
          '#type' => 'select',
          '#title' => $this->t($label),
          '#options' => $fields,
          '#default_value' => $config->get($field_name)??'',
          '#description' => "Select the related field on the {$site_name} system for the iContact {$label} field",
        ];
      }
    }

    $form['icontact_settings']['available_fields'] = [
      '#type' => 'hidden',
      '#required' => false,
      '#title' => $this->t('Available Fields'),
      '#default_value' => $config->get('available_fields'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $fiels_data = $this->getIcontactFields();

    $save_config = $this->config('icontact_mappings.settings')
    ->set('email', $values['email']);
    $field_keys = $this->get_field_keys();
    if(!empty($field_keys)) {
      foreach ($field_keys as $key => $field_key) {
        $save_config->set($field_key, $values[$field_key]);
      }
    }
    $save_config->set('available_fields', $fiels_data)
    ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Get icontact fields
   * @return
   */
  public function getIcontactFields() {
    $field_keys = array();
    // Give the API your information
    iContactApi::getInstance()->setConfig(get_api_details());

   // Store the singleton
    $oiContact = iContactApi::getInstance();

    $getContactWithEmail = $oiContact->getContactWithEmail('sujanshrestha.net@gmail.com');
    if(!empty($getContactWithEmail)) {
      $user_fields = $getContactWithEmail[0];
      $field_keys = array_keys((array)$user_fields);
    }
    $fiels_data = serialize($field_keys);
    return $fiels_data;
  }

  /**
   * Get all available field keys on the iContact
   * @return
   */
  public function get_field_keys() {
    $available_field_keys = array();
    $cid = 'icontact_available_fields:' . \Drupal::languageManager()->getCurrentLanguage()->getId();
    if ($cache = \Drupal::cache()->get($cid)) {
      $available_field_keys = $cache->data;
    }
    else {
    // Give the API your information
      iContactApi::getInstance()->setConfig(get_api_details());

   // Store the singleton
      $oiContact = iContactApi::getInstance();
      $icontact_mappings = \Drupal::config('icontact_api.settings');
      if($icontact_mappings->get('app_id')) {
        $api_username = $icontact_mappings->get('api_username');
        $getContactWithEmail = $oiContact->getContactWithEmail($api_username);

        set_icontact_message($oiContact, false);

        if(!empty($getContactWithEmail)) {
          $user_fields = $getContactWithEmail[0];
          $available_field_keys = array_keys((array)$user_fields);
          $available_field_keys = array_diff($available_field_keys, array('email'));
        }
      }

      \Drupal::cache()->set($cid, $available_field_keys);
    }

    return $available_field_keys;
  }
}
