<?php

namespace Drupal\icontact_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Social Autopost.
 */
class AdvancedSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['icontact_integration_advanced.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'icontact_integration.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('icontact_integration_advanced.settings');

    $form['advanced_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced Settings'),
      '#open' => TRUE,
    ];
    $form['advanced_settings']['clear_data'] = array(
      '#type' => 'checkbox',
      '#title' => $this
      ->t('Remove configuration data when uninstalling'),
      '#default_value' => $config->get('clear_data')??'0',
      '#description' => $this->t('Remove module settings and API configurations after program is uninstalled')
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('icontact_integration_advanced.settings')
    ->set('clear_data', $values['clear_data'])
    ->save();

    parent::submitForm($form, $form_state);
  }

}
