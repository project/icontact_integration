<?php

namespace Drupal\icontact_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\user\Entity\Role;
use Drupal\icontact_integration\Plugin\iContactApi;

/**
 * Settings form for Social Autopost.
 */
class UsersSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['icontact_users.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'icontact_users.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('icontact_users.settings');
    $available_lists = array(''=>'None');

    $cid = 'icontact_available_lists:' . \Drupal::languageManager()->getCurrentLanguage()->getId();
    if ($cache = \Drupal::cache()->get($cid)) {
      $available_lists = $cache->data;
    }
    else {
      // Give the API your information
      iContactApi::getInstance()->setConfig(get_api_details());

      // Store the singleton
      $oiContact = iContactApi::getInstance();

      set_icontact_message($oiContact,false);

      $getLists = $oiContact->getLists();
      if(!empty($getLists)) {
        foreach ($getLists as $key => $value) {
          $available_lists[$value->listId] = $value->name;
        }
      }
      \Drupal::cache()->set($cid, $available_lists);
    }

    $form['information'] = array(
      '#type' => 'vertical_tabs',
    );

    $all_user_roles = all_user_roles();

    $count_roles = count($all_user_roles);
    $role_priorities = array();
    for ($i=1; $i <= $count_roles; $i++) {
      $role_priorities[$i] = ucfirst(number_to_word($i));
    }

    foreach ($all_user_roles as $role_key => $entitty_bundle) {
      if($role_key != 'anonymous') {
        $label = $entitty_bundle->label();

        $form[$role_key] = array(
          '#type' => 'details',
          '#title' => $this
          ->t($label),
          '#group' => 'information',
          '#description' => 'General configurations for entity bundles',
        );

        $form[$role_key][$role_key.'_active'] = array(
          '#type' => 'checkbox',
          '#title' => $this
          ->t('Enable '.$label.' iContact subscription'),
          '#default_value' => $config->get($role_key.'_active')??'0',
        );

        $available_subscription_options = available_subscription_options();
        // $available_unsubscription_options = available_unsubscription_options();
        $subscribe_when  = $config->get($role_key.'_subscribe_when')??[];
        $unsubscribe_when  = $config->get($role_key.'_unsubscribe_when')??[];
        $list_id  = $config->get($role_key.'_list_id')??[];
        $weight  = $config->get($role_key.'_weight')??[];

        $form[$role_key][$role_key.'_subscribe_when'] = array(
          '#type' => 'checkboxes',
          '#options' => $available_subscription_options,
          '#title' => $this->t('Subscribe '.$label.' on the iContact: '),
          '#default_value' => $subscribe_when,
          '#description' => t('Select available opions for user subscription'),
        );

        // $form[$role_key][$role_key.'_unsubscribe_when'] = array(
        //   '#type' => 'checkboxes',
        //   '#options' => $available_unsubscription_options,
        //   '#title' => $this->t('Un-subscribe '.$label.' on the iContact: '),
        //   '#default_value' => $unsubscribe_when,
        //   '#description' => t('Select available opions for user un-subscription'),
        // );

        $form[$role_key][$role_key.'_list_id'] = array(
          '#type' => 'select',
          '#options' => $available_lists,
          '#title' => $this->t('Subscribe '.$label.' on the iContact list '),
          '#default_value' => $list_id,
          '#description' => t('Select available opions for user subscription'),
        );

        $form[$role_key][$role_key.'_weight'] = array(
          '#type' => 'select',
          '#options' => $role_priorities,
          '#title' => $this->t('Weight'),
          '#default_value' => $weight,
          '#description' => t("Select the priority for the role <em>{$label}</em> if the user have multiple roles."),
        );
      }
    }
    $form['#attached'] = ['library' => ['icontact_integration/icontact_integration']];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $get_entitty_bundles = get_entitty_bundles();
    $config = $this->config('icontact_users.settings');

    $all_user_roles = all_user_roles();

    foreach ($all_user_roles as $role_key => $entitty_bundle) {
      if($role_key != 'anonymous') {
        $label = $entitty_bundle->label();

        $active = $values[$role_key.'_active'];
        $weight = $values[$role_key.'_weight'];
        $subscribe_when = $values[$role_key.'_subscribe_when'];
        // $unsubscribe_when = $values[$role_key.'_unsubscribe_when'];
        $list_id = $values[$role_key.'_list_id'];

        $subscribe_when = array_filter($subscribe_when);
        // $unsubscribe_when = array_filter($unsubscribe_when);

        $config->set($role_key.'_active', $active);
        $config->set($role_key.'_weight', $weight);
        $config->set($role_key.'_subscribe_when', $subscribe_when);
        // $config->set($role_key.'_unsubscribe_when', $unsubscribe_when);
        $config->set($role_key.'_list_id', $list_id);
      }
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }



}
