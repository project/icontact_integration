<?php

namespace Drupal\icontact_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\icontact_integration\Plugin\iContactApi;

/**
 * Settings form for Social Autopost.
 */
class ApiKeysSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['icontact_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'icontact_api.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('icontact_api.settings');
    $help = Url::fromRoute('icontact_integration.help_guide',array(),array('absolute'=>'true'))->toString();

    $form['icontact_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('iContact API Configurations'),
      '#open' => TRUE,
      '#description' => $this->t('Note : You have to create a iContact application before filling the following details. Click <a href="https://developers.icontact.com/apps">here</a> to create new iContact application. '),
    ];

    $form['icontact_settings']['app_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('App Id'),
      '#default_value' => $config->get('app_id'),
      '#description' => $this->t('Your icontact App ID.'),
    ];

    $form['icontact_settings']['api_password'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('App Password'),
      '#default_value' => $config->get('api_password'),
      '#description' => $this->t('Your icontact App Password'),
    ];

    $form['icontact_settings']['api_username'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('API Username'),
      '#default_value' => $config->get('api_username'),
      '#description' => $this->t('Your icontact page id. Click <a href="https://www.icontact.com/business/help/2814101678867149">here</a> to get details about the icontact page id. '),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Delete custom caches
    delete_custom_caches();

    $this->config('icontact_api.settings')
    ->set('app_id', $values['app_id'])
    ->set('api_password', $values['api_password'])
    ->set('api_username', $values['api_username'])
    ->save();

    parent::submitForm($form, $form_state);
  }


  public function numToOrdinalWord($num)
  {
    $first_word = array('eth','First','Second','Third','Fouth','Fifth','Sixth','Seventh','Eighth','Ninth','Tenth','Elevents','Twelfth','Thirteenth','Fourteenth','Fifteenth','Sixteenth','Seventeenth','Eighteenth','Nineteenth','Twentieth');
    $second_word =array('','','Twenty','Thirthy','Forty','Fifty');

    if($num <= 20)
      return $first_word[$num];

    $first_num = substr($num,-1,1);
    $second_num = substr($num,-2,1);

    return $string = str_replace('y-eth','ieth',$second_word[$second_num].'-'.$first_word[$first_num]);
  }

}
