DESCRIPTION:
------------
iContact allows businesses, non-profit organizations, and associations to easily create, send, and track email newsletters, surveys, and autoresponders. The latest release of this Drupal module utilizes iContact’s 2.0 API allowing for management of iContact accounts from your Drupal powered web site as well as allowing new subscriptions by users and message statistics.

INSTALLATION:
-------------
1. Extract the tar.gz into your 'modules' or directory.
2. Install the module at extend' section.


